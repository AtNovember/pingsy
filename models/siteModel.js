var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var siteSchema = new Schema({
  title: {type:String},
  url: {type:String},
  email: {type:String},
  created_at: {type:Date},
  updated_at: {type:Date}
});

var Site = mongoose.model('Site', siteSchema);
module.exports = Site;
