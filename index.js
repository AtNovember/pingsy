var mongoose = require('mongoose');
mongoose.connect('mongodb://mgbeta.ru:10098');
// mongoose.connect('mongodb://localhost:27017/sites');
var Site = require('./models/siteModel');

var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({
    extended: true
})); // support encoded bodies
app.use(bodyParser.json()); // support json encoded bodies

var request = require('request');
var _ = require('lodash');

app.set('view engine', 'jade');

var siteRouter = require('./Routes/siteRoutes')(Site);

app.use('/', siteRouter);
// app.use('/:siteId', siteRouter);
// app.use('/:siteId/update', siteRouter);
// app.use('/delete/:siteId', siteRouter);

// app.get('/', function(req, res){
//   res.render('index', {sites: sites});
// });

// add sender
var nodemailer = require('nodemailer');
var config = {
    nodemailer: require('./config/nodemailer')
};
var transporter = nodemailer.createTransport(config.nodemailer.transport);

// var timeout = 5 * 60 * 1000;
var timeout = 3 * 1000;


// ping the server
function pingsyServer() {

    // get all the sites`
    Site.find({}, function(err, sites) {
        if (err) throw err;
          // console.log(sites);
        _.forEach(sites, function(site, i) {

            var email = site.email;

            if (Array.isArray(email)) {
                email = email.join(", ");
            }

            request(site.url, function(err, response, body) {
                var message;

                if (err) {
                    message = "Произошла ошибка: " + err;
                    return;
                }

                if (response === null) {
                    message = "Ваш ресурс " + site.url + " не отвечает \nВозможно, вы указали неверный URL";
                } else if (response && response.statusCode >= 400 && response.statusCode < 600) {
                    message = "ВНИМАНИЕ! Сайт " + site.url + " не работает! \nКод ошибки: " + response.statusCode;
                } else {
                    message = "Все хорошо! Сайт " + site.url + " работает! \nКод ответа: " + response.statusCode; // Show the HTML for the Google homepage.
                    return;
                }

                // setup e-mail data with unicode symbols
                var options = _.merge(config.nodemailer.options, {
                    to: email, // list of receivers
                    subject: 'Wake Up, Neo! Наш сайт: ' + site.url + ' работает некорректно!', // Subject line
                    text: 'Упал сайт \n' + message, // plaintext body
                    html: '<b>Сайт упал:</b>\n' + message + " <br /> Дата проверки " + Date.now() + " "// html body
                });

                console.log("options", options);

                // TODO отправка сообщений
                transporter.sendMail(options, function(err, info) {
                  if (err) return console.log(err);
                  console.log('Message sent: ' + info.response);
                });

            });
        });
    });


}

function TimeoutHandler() {
    pingsyServer();
    clearTimeout(timeoutId);
    // console.log ('I`m a handler CLEAR');
}

timeoutId = setInterval(TimeoutHandler, timeout);

app.listen(3000, function() {
    console.log('Example app listening on port 3000!');
});
