var express = require('express');
var app = express();
var bodyParser = require('body-parser');
// app.use(express.bodyParser());
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
  extended: true
})); // support encoded bodies
