var express = require('express');

var routes = function(Site) {
    var siteRouter = express.Router();

    siteRouter.route('/')
        .post(function(req, res) {
            var newSite = new Site(req.body);
            newSite.save();
            console.log('req.body', newSite);
            res.status(201).send(newSite);
        })
        .get(function(req, res) {
            Site.find({}, function(err, sites) {
                if (err) throw err;
                return res.render('index', {
                    sites: sites
                });
            });
        });

        siteRouter.route('/add')
            .post(function(req, res) {
                var newSite = new Site(req.body);
                newSite.save();
                console.log('req.body', newSite);
                res.status(201).send(newSite);
            })
            .get(function(req, res) {
                Site.find({}, function(err, sites) {
                    if (err) throw err;
                    return res.render('site');
                });
            });

    siteRouter.use('/:siteId', function(req, res, next) {
        Site.findById(req.params.siteId, function(err, site) {
            if (err) {
                res.status(500).send(err);
            } else if (site) {
                req.site = site;
                next();
            } else {
                res.status(404).send('Сайт отсутсвует в базе, либо уже удален');
            }
        });
    });

    siteRouter.route('/:siteId')
        .get(function(req, res) {
            return res.render('site', {
                site: req.site
            });
        })
        .post(function(req, res) {
            req.site.title = req.body.title;
            req.site.url = req.body.url;
            req.site.email = req.body.email;
            req.site.save(function(err) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    // res.json(req.site);
                    return res.render('site', {
                        site: req.site
                    });
                }
            });
        });

    siteRouter.route('/:siteId/delete')
        .get(function(req, res) {
            req.site.remove(function(err) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.status(204).send('Removed');
                    console.log('Site DETETED');
                }
            });
        });
    return siteRouter;
};

module.exports = routes;
